<?php

	$db_config = array(
		array( 'id'=>0, 'name'=>'altitude', 'type'=>'MySQL', 'hostname'=>'127.0.0.1', 'username'=>'username', 'password'=>'password', 'port'=>3306),
		array( 'id'=>1, 'name'=>'william_grant', 'type'=>'MySQL', 'hostname'=>'127.0.0.1', 'username'=>'root', 'password'=>'Welcome2Forte', 'port'=>3306),
		array( 'id'=>2, 'name'=>'forteis', 'type'=>'MySQL', 'hostname'=>'127.0.0.1', 'username'=>'db_user', 'password'=>'Welcome2Forte', 'port'=>3306),
		array( 'id'=>3, 'name'=>'My Database3', 'type'=>'MySQL', 'hostname'=>'127.0.0.1', 'username'=>'db_user', 'password'=>'my_secret', 'port'=>3306 ),
		array( 'id'=>4, 'name'=>'An Oracle Db', 'type'=>'Oracle', 'hostname'=>'127.0.0.1', 'username'=>'db_user', 'password'=>'my_secret', 'port'=>4480 ),
		array( 'id'=>5, 'name'=>'Another Oracle', 'type'=>'Oracle', 'hostname'=>'127.0.0.1', 'username'=>'db_user', 'password'=>'my_secret', 'port'=>550),
		array( 'id'=>6, 'name'=>'Vehicle Fleet', 'type'=>'Firebird', 'hostname'=>'127.0.0.1', 'username'=>'db_user', 'password'=>'my_secret', 'port'=>3050 ),
		array( 'id'=>7, 'name'=>'psql_test', 'type'=>'PgSQL', 'hostname'=>'localhost', 'username'=>'postgres', 'password'=>'my_secret', 'port'=>5432 ),
		array( 'id'=>8, 'name'=>'cms', 'type'=>'PgSQL', 'hostname'=>'localhost', 'username'=>'postgres', 'password'=>'my_secret', 'port'=>5432 ),
		array( 'id'=>9, 'name'=>'Customers', 'type'=>'SQLite', 'path'=>'/home/kevin/db.sqlite')
		);
