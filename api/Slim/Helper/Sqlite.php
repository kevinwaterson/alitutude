<?php

class Sqlite extends Db{

	public function __construct( array $config )
	{
		parent::__construct( $config );
	}


	/**
	*
	* Fetch Tables from database
	*
	*/
	public function getTables()
	{
		$sql = "SELECT * FROM dbname.sqlite_master WHERE type='table'";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$res = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $res;
	}
}
