<?php

class Firebird extends Db{

	public function __construct( array $config )
	{
		parent::__construct( $config );
	}


	/**
	*
	* Fetch Tables from database
	*
	*/
	public function getTables()
	{
		$sql = 'SELECT RDB$RELATION_NAME
			FROM RDB$RELATIONS
			WHERE RDB$SYSTEM_FLAG = 0
			AND RDB$VIEW_BLR IS NULL
			ORDER BY RDB$RELATION_NAME';
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$res = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $res;
	}


	/**
	*
	* Fetch fields and data from table
	*
	* @access       public
	* @param	string  $table_name
	* @return       array
	*
	*/
	public function getData( $table_name, $limit=null, $offset=null )
	{
		$sql = "SELECT * FROM `$table_name`";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$data = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $data;
	}

}
