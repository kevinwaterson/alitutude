<?php


class Db extends pdo{

	public function __construct( array $config )
	{
		$db_type = strtolower( $config['type'] );
		$db_port = $config['port'];
		$db_name = $config['name'];
		$hostname = $config['hostname'];
		$username = $config['username'];
		$password = $config['password'];
		parent::__construct( "$db_type:host=$hostname;dbname=$db_name;port=$db_port", $username, $password );
	}


	/**
	*
	* Fetch fields and data from table
	*
	* @access	public
	* @param	string	$table_name
	* @return	array
	*
	*/
	public function getData( $table_name, $limit=null, $offset=null )
	{
		$sql = "SELECT * FROM $table_name";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$data = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $data;
	}
}
