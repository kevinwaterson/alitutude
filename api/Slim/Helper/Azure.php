<?php

class Azure extends Db{

	public function __construct( array $config )
	{
		parent::__construct( $config );
	}


	/**
	*
	* Fetch Tables from database
	*
	*/
	public function getTables()
	{
		$sql = "select t.name AS table_name
			FROM sys.tables t
			JOIN sys.dm_db_partition_stats s
			ON t.object_id = s.object_id
			AND t.type_desc = ‘USER_TABLE’
			AND t.name not like ‘%dss%’
			AND s.index_id = 1";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$res = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $res;
	}


	/**
	*
	* Fetch fields and data from table
	*
	* @access       public
	* @param	string  $table_name
	* @return       array
	*
	*/
	public function getData( $table_name, $limit=null, $offset=null )
	{
		$sql = "SELECT * FROM `$table_name`";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$data = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $data;
	}

}
