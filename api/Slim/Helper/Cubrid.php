<?php

class Cubrid extends Db{

	public function __construct( array $config )
	{
		parent::__construct( $config );
	}


	/**
	*
	* Fetch Tables from database
	*
	*/
	public function getTables()
	{
		$sql = "SHOW FULL TABLES as table_name WHERE table_type = 'BASE TABLE'";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$res = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $res;
	}


	/**
	*
	* Fetch fields and data from table
	*
	* @access       public
	* @param	string  $table_name
	* @return       array
	*
	*/
	public function getData( $table_name, $limit=null, $offset=null )
	{
		$sql = "SELECT * FROM `$table_name`";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$data = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $data;
	}

}
