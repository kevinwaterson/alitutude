<?php

class Mysql extends Db{

	public function __construct( array $config )
	{
		parent::__construct( $config );
	}


	/**
	*
	* Fetch Tables from database
	*
	*/
	public function getTables()
	{
		$sql = "SELECT TABLE_NAME AS table_name FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE'";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$res = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $res;
	}


	/**
	*
	* Fetch fields and data from table
	*
	* @access       public
	* @param	string  $table_name
	* @return       array
	*
	*/
	public function getData( $table_name, $limit=null, $offset=null )
	{
		$sql = "SELECT * FROM `$table_name`";
		$stmt = $this->prepare( $sql );
		$stmt->execute();
		$data = $stmt->fetchAll( PDO::FETCH_ASSOC );
		return $data;
	}

}
