<?php

include 'config/config.php';
include 'Slim/Helper/Db.php';
include 'Slim/Helper/Mysql.php';
include 'Slim/Helper/Pgsql.php';

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->config(array(
	'debug' => true,
	'templates.path' => './templates',
	'db_config' => $db_config
));


$view = $app->view();
$view->setTemplatesDirectory('./templates');

$app->get('/hello/:name', function ($name) {
	    echo "Hello, $name";
});


$app->get('/', function( ){

	include 'config/config.php';
	echo json_encode( $db_config );
});

$app->get( '/getTables/:dbId', function( $database_id ){

	include 'config/config.php';
	$config = $db_config[$database_id];
	$type = strtolower( $config['type'] );
	// $port = $config['port'];
	// $name = $config['name'];
	// $username = $config['username'];
	// $hostname = $config['hostname'];
	// $password = $config['password'];

	try{
		// $dbh = new PDO( "$type:host=$hostname;dbname=$name;port=$port", $username, $password );
		// $sql = "SELECT TABLE_NAME AS table_name FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE'";
		// $stmt = $dbh->prepare( $sql );
		// $stmt->execute();
		// $tables = $stmt->fetchAll( PDO::FETCH_ASSOC );

		$db = new $type( $config );
		$tables = $db->getTables();
		render( $tables );
	}
	catch( Exception $e )
	{
		// alert message
	}
});

/*
$app->get('/getTableFields/:id', function( $table_id ){
        $array = array(
                        array( 'id'=>1, 'name'=>'Susan', 'gender'=>'F', 'age'=>27 ),
                        array( 'id'=>2, 'name'=>'Billy', 'gender'=>'M', 'age'=>38 ),
                        array( 'id'=>3, 'name'=>'Bob', 'gender'=>'M', 'age'=>83 ),
                        array( 'id'=>4, 'name'=>'Ellen', 'gender'=>'F', 'age'=>33 ),
                        array( 'id'=>5, 'name'=>'Pat', 'gender'=>'F', 'age'=>12 ),
                        array( 'id'=>6, 'name'=>'Greg', 'gender'=>'M', 'age'=>22 ),
                        array( 'id'=>7, 'name'=>'Shirley', 'gender'=>'F', 'age'=>53 ),
                        array( 'id'=>8, 'name'=>'Anne', 'gender'=>'F', 'age'=>39 ),
                        array( 'id'=>9, 'name'=>'Christine', 'gender'=>'F', 'age'=>73 ),
                        array( 'id'=>10, 'name'=>'Peter', 'gender'=>'M', 'age'=>29 ),
                        array( 'id'=>11, 'name'=>'Alfred', 'gender'=>'M', 'age'=>18 ),
                        array( 'id'=>12, 'name'=>'Anne Marie', 'gender'=>'F', 'age'=>37 ),
                        array( 'id'=>13, 'name'=>'Joanne', 'gender'=>'F', 'age'=>38 ),
                        array( 'id'=>14, 'name'=>'Mary', 'gender'=>'F', 'age'=>29 ),
                        array( 'id'=>15, 'name'=>'Joseph', 'gender'=>'M', 'age'=>63 ),
                );

	render( $array );
});

*/

$app->get('/getTableFields/:database_id/:tableName', function( $database_id, $table_name){

        include 'config/config.php';

        $config = $db_config[$database_id];
        $type = strtolower( $config['type'] );
        // $port = $config['port'];
        // $name = $config['name'];
        // $username = $config['username'];
        // $hostname = $config['hostname'];
        // $password = $config['password'];

        try{
                $db = new $type( $config );
                // $dbh = new PDO( "$type:host=$hostname;dbname=$name;port=$port", $username, $password );
                // $sql = "SELECT * FROM $tableName";
                // $stmt = $db->prepare( $sql );
                // $stmt->execute();
                // $data = $stmt->fetchAll( PDO::FETCH_ASSOC );
                // $tables = array( 'users', 'customers', 'clients', 'suppliers', 'contacts', $config['name'] );
		$data = $db->getData( $table_name );
                render( $data );
        }
        catch( Exception $e )
        {
                // alert message
        }


        // echo json_encode( $array );
});

function render( $array ){
	 echo json_encode( $array );
}

$app->run();
