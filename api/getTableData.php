<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->config(array(
	'debug' => true,
	'templates.path' => './templates',
));


$view = $app->view();
$view->setTemplatesDirectory('./templates');

$app->get('/hello/:name', function ($name) {
	    echo "Hello, $name";
});


$app->get('/api/getTableFields/:tableName', function( $tableName){

        include 'config/config.php';

        $config = $db_config[$database_id];
        $type = strtolower( $config['type'] );
        $port = $config['port'];
        $name = $config['name'];
        $username = $config['username'];
        $hostname = $config['hostname'];
        $password = $config['password'];

        try{
                $dbh = new PDO( "$type:host=$hostname;dbname=$name;port=$port", $username, $password );
                $sql = "SELECT * FROM `$tableName`";
                $stmt = $dbh->prepare( $sql );
                $stmt->execute();
                $data = $stmt->fetchAll( PDO::FETCH_ASSOC );
                /// $tables = array( 'users', 'customers', 'clients', 'suppliers', 'contacts', $config['name'] );

                render( $data );
        }
        catch( Exception $e )
        {
                // alert message
        }


	echo json_encode( $array );
});


$app->run();
