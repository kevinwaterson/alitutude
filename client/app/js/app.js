var altitude = angular.module('altitude', ['ui.bootstrap', 'ngRoute', 'trNgGrid', 'ngCookies']);

	// configure our routes
	altitude.config(function($routeProvider, $locationProvider) {
	
	// set html5 mode 
	$locationProvider.html5Mode(true);

	$routeProvider
		// route for the home page
		.when('/', {
			templateUrl : '/client/app/partials/index.html',
			controller  : 'ApplicationCtrl'
		})

		// route for the about page
		.when('/about', {
			templateUrl : '/client/app/partials/about.html',
			controller  : 'AboutCtrl'
		})

		// route for the contact page
		.when('/tables', {
			templateUrl : '/client/app/partials/tables.html',
			controller  : 'TableFieldCtrl'
		})
		 .when('/databases/:dbId/tables/:tableId', { 
			templateUrl: '/client/app/partials/tables.html', 
			controller: 'TableCtrl' 
		})
		 .when('/login', { 
			templateUrl: '/client/app/partials/login.html', 
			controller: 'LoginCtrl' 
		})
		.otherwise({ 
			redirectTo: '/client'
		});
	});


function ApplicationCtrl($scope, $http) {

  // load the db names
  $http.get('/api/index.php').
     success(function(data) {
     $scope.databases = data;
  });

  $scope.status = {
    isFirstOpen: true,
    isFirstDisabled: false
  };

  $scope.loadTableNames = function(db) {
  $http.get('/api/getTables/'+db.id).
	success(function(tdata) {
	// $scope.tableData = tdata;
	db.tables=tdata;
	});
    }

    $scope.message = 'this is the index';
/*
  $scope.loadTableFields = function( db, tableName ) {
  $http.get('http://altitude.dev/api/getTableFields/'+tableName).
	success(function(tfields) {
	// $scope.tableData = tdata;
	db.tableFields=tfields;
	});
    }
*/
}


function AboutCtrl( $scope, $http) {
	$scope.message = 'this is the about stuff';
}

function TableCtrl($scope, $http, $routeParams) {
	// $http.get('/api/getTableFields/0/quotes').
	$http.get('/api/getTableFields/' + $routeParams.dbId + '/' + $routeParams.tableId).
	success(function(tfields) {
		$scope.numRecords = tfields.length;
		$scope.tableFields = tfields;
	});
}

function LoginCtrl( $scope, $http) {
        $scope.message = 'this is the login screen...';
}

